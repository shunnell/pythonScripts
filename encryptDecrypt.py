#we first generate a encryption key using Fernet.generate_key(). This key is then saved into a file named key.key for later use. In the encrypt() function, we use the Fernet library to generate a cipher object with the key, which can be used to encrypt the file data. Similarly in the decrypt() function, we first read the encrypted data from the file, and use the same key to decrypt it.
#
#To encrypt and decrypt multiple files, we can loop over the list of filenames and call the encrypt() and decrypt() functions for each file. We can also use the os library to recursively traverse a directory and encrypt or decrypt all files in it.


import os
from cryptography.fernet import Fernet

# Generate a encryption key
key = Fernet.generate_key()

# Save the key into a file for later use
with open('key.key', 'wb') as file:
    file.write(key)

# Function to encrypt a file
def encrypt(filename, key):
    f = Fernet(key)

    with open(filename, 'rb') as file:
        # Read the file data
        data = file.read()

    # Encrypt the data
    encrypted_data = f.encrypt(data)

    with open(filename, 'wb') as file:
        # Write the encrypted data to the file
        file.write(encrypted_data)

# Function to decrypt a file
def decrypt(filename, key):
  f = Fernet(key)

  with open(filename, 'rb') as file:
      # Read the file data
      encrypted_data = file.read()

  # Decrypt the data
  decrypted_data = f.decrypt(encrypted_data)

  with open(filename, 'wb') as file:
      # Write the decrypted data to the file
      file.write(decrypted_data)

# Example usage:
# Encrypt a file
encrypt('myfile.txt', key)

# Decrypt a file
decrypt('myfile.txt', key)

